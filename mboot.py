#!/usr/bin/env python

import optparse
import serial
import sys
import threading
import time

def log(s):
    sys.stdout.write(s + "\n")
    sys.stdout.flush()

class WriterThread(threading.Thread):
    def __init__(self, sp, data):
        threading.Thread.__init__(self)
        self.sp = sp
        self.data = data

    def run(self):
        sp = self.sp
        data = self.data
        BS = 512
        i = 0
        n = len(data)
        while i < n:
            log("W: %04X/%04X" % (i, n))
            sp.write( data[i:i+BS] )
            sp.flush()
            i += BS

class UploadThread(WriterThread):
    def __init__(self, sp, data):
        WriterThread.__init__(self, sp, data)

    def run(self):
        WriterThread.run(self)
        time.sleep(0.1)
        self.sp.baudrate = 115200

def calc_checksum(data):
    a = 0
    for c in data:
        a = (a + ord(c)) & 0xFF
    return a

def load_file(filename):
    data = open(filename, "rb").read()
    size = len(data)
    checksum = calc_checksum(data)
    return "%c%c%c%c" % ( (size >>  0) & 0xFF,
                          (size >>  8) & 0xFF,
                          (size >> 16) & 0xFF,
                          (size >> 24) & 0xFF ) + \
            data + \
            "%c%c" % (0, checksum)

def main():
    op = optparse.OptionParser()
    op.add_option("-d", "--device", help="Serial port device (default: /dev/ttyUSB0)", dest="dev", default="/dev/ttyUSB0")
    op.add_option("-u", "--uart-code", help="UART-boot code (default: UART.nb0)", dest="uart_code", default="UART.nb0")
    op.add_option("-m", "--mboot-code", help="MBOOT code (default: polluxb)", dest="mboot_code", default="polluxb")

    opts, args = op.parse_args()

    print("Reading UART boot code...")
    uart_data = open(opts.uart_code, "rb").read()
    if len(uart_data) != 16 * 1024:
        print("ERROR: Filesize is not 16K")
        sys.exit(1)

    print("Reading MBOOT code...")
    mboot_data = load_file(opts.mboot_code)

    sp = serial.Serial(opts.dev, 19200, timeout=1)
    if not sp.isOpen():
        print("ERROR: Serial port not open")
        sys.exit(1)

    log("Press Ctrl-C to quit")

    log("Uploading UART boot code...")
    ut = UploadThread(sp, uart_data)
    ut.start()

    def is_printable(c):
        return ord(c) >= 0x20 and ord(c) <= 0x7E

    def is_newline(c):
        return ord(c) == 0x0A

    def get_printable(c):
        return "'%c'" % (c,) if is_printable(c) else "<LF>" if is_newline(c) else ""

    S_IDLE = 0
    S_REQUEST_MBOOT = 1
    S_RUN_MBOOT = 2

    state = S_IDLE

    line = ""

    while True:
        try:
            for c in sp.read(max(1, sp.inWaiting())):
                log("R: %02X %s" % (ord(c[0]), get_printable(c[0])))
                if is_printable(c) or is_newline(c):
                    if line.endswith("\n"): line = ""
                    line += c
                else:
                    # Reset line on garbage read...
                    line = ""

            if line == "Select operation (1~5)":
                line = ""
                if state == S_IDLE:
                    state = S_REQUEST_MBOOT
                    ut.join()
                    log("Request MBOOT")
                    wt = WriterThread(sp, "2")
                    wt.start()
                elif state == S_REQUEST_MBOOT:
                    state = S_RUN_MBOOT
                    log("Run MBOOT")
                    wt = WriterThread(sp, "3")
                    wt.start()
            elif line == " Now Downloading !!\n":
                line = ""
                log("Uploading MBOOT")
                wt = WriterThread(sp, mboot_data)
                wt.start()

        except KeyboardInterrupt:
            break

    sp.close()

if __name__ == "__main__":
    main()
